# Warning! Is Not Yet Ready!!!


# My Job is to modify it accordingly and use zenity or even better with Object Pascal + Bash, for Artix Linux Server.
# Licensing is free for Object Pascal, when binaries are produced, so I may use MIT too. 

##`Applications developed with Lazarus
The GPL does not cover any application binary created with Lazarus. The application binary itself is only limited by the components you actually link to, and in the FPC/Lazarus project those are all LGPL_with_exception. So though potentially confusing this licensing is not a problem when developing binaries with Lazarus, even if you have commercial components with designtime parts. This because you only have to open the source when you distribute the result (i.e. Lazarus with preinstalled components).`
##https://wiki.lazarus.freepascal.org/licensing

# For this purpose a new GitLab Project Page will be created.

Automated-LAMP-Installation
===========================

This shell script was about the installation (and uninstallation) of the LAMP Stack automatically on Ubuntu server


How to use it:
-------------

Download the following shell script on your Ubuntu server or clone it with the following command.
```
git clone https://github.com/arbabnazar/Automated-LAMP-Installation.git
```
Give the execution right to it.
------------------------------
```
chmod +x install.sh
```
Then run it as sudo user.
-------------------------
```
sudo ./install.sh
```


How to uninstall:
-------------

Give the execution right to it.
------------------------------
```
chmod +x uninstall.sh
```
Then run it as sudo user.
-------------------------
```
sudo ./uninstall.sh
```
